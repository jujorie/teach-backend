# Backend Buid step by step

Now is time to put some work o the model. Add hooks to crypt the password of the user


##  Add Crypt dependency

Install [bcrypt](https://www.npmjs.com/package/bcrypt) to crypt data

```sh
$ npm i bcrypt -S
```
##  Modify Model

Go to __/src/models__ and modify __user.js__

```javascript
'use strict';
module.exports = (sequelize, DataTypes) => {
  var bcrypt = require('bcrypt');

  function cryptPassword(user) {
    return new Promise((resolve, reject)=>{
      if (user.changed('password')) {
        bcrypt.hash(user.password, 10, (err, data) => {
          if(err){
            reject(err);
          } else {
            user.password = data;
            resolve();
          }
        })
      } else {
        resolve();
      }
    })
  }

  var user = sequelize.define(
    'user',
    {
      name: DataTypes.STRING,

      /* For modify model */
      email: {
        type: DataTypes.STRING,
        unique: true
      },

      password: DataTypes.STRING
    },
    {
      hooks: {
        beforeCreate: (user, options) => {
          return cryptPassword(user);
        },
        beforeUpdate: (user, options) => {
          return cryptPassword(user);
        }
      }
    }
  );
  user.associate = function (models) {
    // associations can be defined here
  };
  return user;
};
```
Now with any change of the user then the password will be cyphered

You can now use __Postman__ to create a new user refer to the previous document
in [here](readme.md)

## Implement a basic login method

Let´s modify the swagget file to add a login address

```json
{
    "swagger": "2.0",
    "info": {
        "title": "Demo",
        "version": "1.0.0"
    },
    "produces": [
        "application/json"
    ],
    "consumes": [
        "application/json"
    ],
    "basePath": "/api",
    "paths": {
        "/version": {
            "x-swagger-router-controller": "versionController",
            "get": {
                "operationId": "getVersion",
                "responses": {
                    "default": {
                        "description": "error messages",
                        "schema": {
                            "$ref": "#/definitions/error"
                        }
                    }
                }
            }
        },
        "/login": {
            "post": {
                "x-swagger-router-controller": "userController",
                "operationId": "login",
                "parameters": [
                    {
                        "name": "data",
                        "in": "body",
                        "schema": {
                            "type": "object",
                            "required": [
                                "email",
                                "password"
                            ],
                            "properties": {
                                "email": {
                                    "type": "string"
                                },
                                "password": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "User created"
                    }
                }
            }
        },
        "/user": {
            "post": {
                "x-swagger-router-controller": "userController",
                "operationId": "postUser",
                "parameters": [
                    {
                        "name": "data",
                        "in": "body",
                        "schema": {
                            "type": "object",
                            "required": [
                                "name",
                                "email",
                                "password"
                            ],
                            "properties": {
                                "name": {
                                    "type": "string"
                                },
                                "email": {
                                    "type": "string"
                                },
                                "password": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "User created"
                    }
                }
            }
        }
    },
    "definitions": {
        "error": {
            "properties": {
                "message": {
                    "type": "string",
                    "default": "Invalid Request"
                }
            }
        },
        "login": {
            "properties": {
                "email": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                }
            }
        }
    }
}
```

now adapt the user model

```javascript
'use strict';
module.exports = (sequelize, DataTypes) => {
  var bcrypt = require('bcrypt');

  function cryptPassword(user) {
    return new Promise((resolve, reject) => {
      if (user.changed('password')) {
        bcrypt.hash(user.password, 10, (err, data) => {
          if (err) {
            reject(err);
          } else {
            user.password = data;
            resolve();
          }
        })
      } else {
        resolve();
      }
    })
  }

  var user = sequelize.define(
    'user',
    {
      name: DataTypes.STRING,

      /* For modify model */
      email: {
        type: DataTypes.STRING,
        unique: true
      },

      password: DataTypes.STRING
    },
    {
      hooks: {
        beforeCreate: (user, options) => {
          return cryptPassword(user);
        },
        beforeUpdate: (user, options) => {
          return cryptPassword(user);
        }
      }
    }
  );
  user.associate = function associate(models) {
    // associations can be defined here
  };

  user.checkCredentials = function checkCredentials({email, password}) {
    return user
      .findOne({
        where: {
          email
        }
      })
      .then((foundUser)=>{
        if(!foundUser){
          return Promise.reject(new Error('User Not found'));
        } else {
          return foundUser
            .isMatch(password)
            .then((success)=>{
              if(success) {
                return foundUser;
              }
              return null;
            })
        }
      })
  }

  user.prototype.isMatch = function isMatch(password) {
    var userPassword = this.password;
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, userPassword, (err, same) => {
        if (err) {
          reject(err)
        } else {
          resolve(same)
        }
      })
    })
  }

  return user;
};
```

Now you backend can do a simple login


## Exercise

*   Do more complex login using jwt
