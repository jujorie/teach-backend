'use strict';

module.exports.getVersion = function getVersion(req, res){
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
        version: '1.0.0'
    }, null, 2));
}